// const moment = require('momnet');

// Write Javascript code!
const appDiv = document.getElementById('app');
// appDiv.innerHTML = `<h1>JS Starter</h1>`;

const convertButton = document.getElementById('convertButton');
const startDateTime = document.getElementById('startDateTime');
const endDateTime = document.getElementById('endDateTime');

const convertedStartDate = document.getElementById('convertedStartDate');
const convertedEndDate = document.getElementById('convertedEndDate');

const convertedStartTime = document.getElementById('convertedStartTime');
const convertedEndTime = document.getElementById('convertedEndTime');

convertButton.addEventListener('click', convertTime);
function convertTime() {
  if (startDateTime.value == '' && endDateTime.value == '') {
    convertedStartDate.innerHTML = '--/--/--';
    convertedStartTime.innerHTML = '--:--:--';
    convertedEndDate.innerHTML = '--/--/--';
    convertedEndTime.innerHTML = '--:--:--';
    return alert('Please enter values');
  }
  if(endDateTime.value == ''){
    convertedEndDate.innerHTML = '--/--/--';
    convertedEndTime.innerHTML = '--:--:--';
  }
  if (startDateTime.value == ''){
    convertedStartDate.innerHTML = '--/--/--';
    convertedStartTime.innerHTML = '--:--:--';
  }

  let _startDate = moment(startDateTime.value, 'YYYY-MM-DDTHH:mm:ssZ');
  let _endDate = moment(endDateTime.value, 'YYYY-MM-DDTHH:mm:ssZ');

  if(startDateTime.value && _startDate.isValid()){
    convertedStartDate.innerHTML = _startDate.format('DD/MM/YYYY');
    convertedStartTime.innerHTML = _startDate.format('HH:mm:ss');
  }
  if(endDateTime.value && _endDate.isValid()){
    convertedEndDate.innerHTML = _endDate.format('DD/MM/YYYY');
    convertedEndTime.innerHTML = _endDate.format('HH:mm:ss');
  }
}
